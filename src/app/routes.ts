import { Routes } from '@angular/router';
import { BoutiqueComponent } from './boutique/boutique.component';

export const routes: Routes = [
    { path: '', component: BoutiqueComponent, pathMatch: 'full' },
    { path: 'shop', loadChildren: () => import('./shop/shop.module').then(m => m.ShopModule) },
    { path: '**', component: BoutiqueComponent }
];